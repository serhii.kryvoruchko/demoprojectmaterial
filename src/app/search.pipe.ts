import { Pipe, PipeTransform } from '@angular/core';
import { Post } from './post';

@Pipe({
  name: 'searchNamePosts'
})
export class SearchNamePipe implements PipeTransform {
  transform(posts: Post[], searchName = ''): Post[] {
    if(!searchName.trim()){
      return posts
    }
    return posts.filter((post) =>{
      return post.name.toLowerCase().includes(searchName.toLowerCase())
    })
  }
}

@Pipe({
  name: 'searchAuthorPosts'
})
export class SearchAuthorPipe implements PipeTransform {

  transform(posts: Post[], searchAuthor = ''): Post[] {
    if(!searchAuthor.trim()){
      return posts
    }
    return posts.filter((post) =>{
      return post.author.toLowerCase().includes(searchAuthor.toLowerCase())
    })
  }
}

@Pipe({
  name: 'searchPricePosts'
})
export class SearchPricePipe implements PipeTransform {
  transform(posts: Post[], searchPrice = ''): Post[] {
    if(!searchPrice.trim()){
      return posts
    }
    return posts.filter((post) =>{
      return post.name === searchPrice
    })
  }
}
