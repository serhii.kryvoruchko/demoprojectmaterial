import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, Observable } from "rxjs";
import { environment } from 'src/environments/environment';
import { Post } from './post';


const url:string = 'https://api.medzakupivli.com/angular/regions/?access_token=kRfXiJXtDaEmX6anRHIrmdcS';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(private http:HttpClient) { }
  getAll():Observable<any>{
    return this.http.get(`${environment.url}`)
    // .pipe(
    //   map((response:{[key:string]:any})=>{
    //     return Object
    //     .keys(response)
    //     .map(key =>({
    //       ...response[key],
    //       id:key
    //     }))
        
    //   })
    // )
  }
}
