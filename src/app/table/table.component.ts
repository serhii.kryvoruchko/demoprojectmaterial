import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { of } from 'rxjs';
import { Post } from '../post';
import { TableService } from '../table.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  postProps: string[] = ['name', 'author', 'price'];
  posts:Post[];
  postsDataSource: MatTableDataSource<Post>;
  currentSearchField = '';
  searchForm =  new FormGroup({
    name: new FormControl(''),
    author: new FormControl(''),
    price: new FormControl('')
  });

  constructor(private tableService: TableService) {
    this.posts = [];
    this.postsDataSource = new MatTableDataSource();
  }

  ngOnInit(): void {
    this.tableService.getAll().subscribe((r)=>{
      this.posts = r.data
      this.posts[0].author = "valera";
      this.postsDataSource.data = this.posts;
      console.log(this.posts)
    })
    this.postsDataSource.filterPredicate = this.createFilterPredicate();
  }

  ngAfterViewInit(): void {
    this.postsDataSource.paginator = this.paginator;
    this.postsDataSource.sort = this.sort;
  }

  createFilterPredicate() {
    return (row: Post, filters: string) => {
      const filterArray = filters.split('$');
      const name = filterArray[0];
      const author = filterArray[1];
      const price = filterArray[2];

      const matchFilter = [];

      const columnName = row.name;
      const columnAuthor = row.author;
      const columnPrice = row.price;

      const customFilterN = columnName
        .toLowerCase()
        .includes(name);
      const customFilterA = columnAuthor
        .toLowerCase()
        .includes(author);
      const customFilterP = columnPrice
        .toLowerCase()
        .includes(price);

      matchFilter.push(customFilterN);
      matchFilter.push(customFilterA);
      matchFilter.push(customFilterP);

      return matchFilter.every(Boolean);
    };
  }

  search() {
    const nameControlValue = this.searchForm.get('name')?.value;
    const authorControlValue = this.searchForm.get('author')?.value;
    const priceControlValue = this.searchForm.get('price')?.value;

    let name = nameControlValue === null ? '' : nameControlValue;
    let author = authorControlValue === null ? '' : authorControlValue;
    let price = priceControlValue === null ? '' :priceControlValue;

    const filterValue = name + '$' + author + '$' + price;
    this.postsDataSource.filter = filterValue.trim().toLowerCase();
  }
}